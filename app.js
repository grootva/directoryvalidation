/**
 * @file entry point for the server
 * @author Arick Grootveld
 */


const express = require('express');
const fs = require('fs');
const bodyParser = require('body-parser');
const helmet = require('helmet');
const config = require('./config.json');

const app = express();
const contentDeliveryServer = express();
const urlencodedParser = bodyParser.urlencoded({ extended: false });

const approved = {
  CSS: [],
  js: [],
  images: [],
  otherDirs: [],
};
const validImageFormats = ['jpg', 'png', 'bmp', 'gif'];

let publicExists = false;
let cssExists = false;
let jsExists = false;
let imagesExists = false;

fs.stat(config.publicDir, (err, stats) => {
  try {
    publicExists = stats.isDirectory;
  } catch (err) {
    throw new Error('Public directory missing');
  }
});
fs.stat(`${config.publicDir}/CSS`, (err, stats) => {
  cssExists = false;
  if (publicExists) {
    cssExists = stats.isDirectory;
    if (!cssExists) {
      throw new Error('CSS directory missing');
    }
  }
});
fs.stat(`${config.publicDir}/js`, (err, stats) => {
  jsExists = false;
  if (publicExists) {
    jsExists = stats.isDirectory;
    if (!jsExists) {
      throw new Error('js directory missing');
    }
  }
});
fs.stat(`${config.publicDir}/images`, (err, stats) => {
  imagesExists = false;
  if (publicExists) {
    imagesExists = stats.isDirectory;
    if (!imagesExists) {
      throw new Error('images directory missing');
    }
  }
});

function interpretRequest(requestInput, approvedFiles, validImageForms) {
  const responseFormat = {
    path: '',
    approved: false,
    exists: false,
    isValid: false,
    approvalFailureReason: '',
  };
  const reqInput = `./public${requestInput}`;
  const reqPath = reqInput.split('/');
  const reqType = ((reqPath[reqPath.length - 1]).split('.'))[1];
  responseFormat.path = reqInput;

  reqPath.shift();
  if ((reqPath.includes('..')) || reqPath.includes('.')) {
    responseFormat.approvalFailureReason = 'nice try';
    return (responseFormat);
  }
  reqPath.unshift('.');


  // CSS checker
  if (reqPath[2] === 'CSS') {
    if ((reqPath.length <= 6) && (reqType === 'css')) {
      responseFormat.isValid = true;
      if (!approvedFiles.CSS.includes(reqInput)) {
        approvedFiles.CSS.push(reqInput);
        responseFormat.approved = true;
      } else {
        responseFormat.approvalFailureReason = 'file already on approved list';
      }
    } else if (reqPath.length <= 6) {
      responseFormat.approvalFailureReason = 'in wrong directory';
    } else if (reqType === 'css') {
      responseFormat.approvalFailureReason = 'filePath too long';
    } else {
      responseFormat.approvalFailureReason = 'filePath too long and in wrong directory';
    }
  }

  // js checker
  else if (reqPath[2] === 'js') {
    if ((reqPath.length <= 6) && (reqType === 'js')) {
      responseFormat.isValid = true;
      if (!approvedFiles.js.includes(reqInput)) {
        approvedFiles.js.push(reqInput);
        responseFormat.approved = true;
      } else {
        responseFormat.approvalFailureReason = 'file already on approved list';
      }
    } else if (reqPath.length <= 6) {
      responseFormat.approvalFailureReason = 'in wrong directory';
    } else if (reqType === 'js') {
      responseFormat.approvalFailureReason = 'filePath too long';
    } else {
      responseFormat.approvalFailureReason = 'filePath too long and in wrong directory';
    }
  }

  // images checker
  else if (reqPath[2] === 'images') {
    if ((reqPath.length <= 6) && (validImageForms.includes(reqType))) {
      responseFormat.isValid = true;
      if (!approvedFiles.images.includes(reqInput)) {
        approvedFiles.images.push(reqInput);
        responseFormat.approved = true;
      } else {
        responseFormat.approvalFailureReason = 'file already on approved list';
      }
    } else if (reqPath.length <= 6) {
      responseFormat.approvalFailureReason = 'in wrong directory';
    } else if (validImageForms.includes(reqType)) {
      responseFormat.approvalFailureReason = 'filePath too long';
    } else {
      responseFormat.approvalFailureReason = 'filePath too long and in wrong directory';
    }
  }

  // Other Dir checker
  else if (!((reqType === 'css') || (reqType === 'js') || (validImageForms.includes(reqType)))) {
    responseFormat.isValid = true;
    if (!approvedFiles.otherDirs.includes(reqInput)) {
      responseFormat.approved = true;
      approvedFiles.otherDirs.push(reqInput);
    } else {
      responseFormat.approvalFailureReason = 'file is already approved';
    }
  } else {
    responseFormat.approvalFailureReason = 'file format should be in other directory';
  }


  if (fs.existsSync(reqInput)) {
    responseFormat.exists = true;
  }
  if (!reqType) {
    responseFormat.approvalFailureReason = 'not a file format';
    responseFormat.exists = false;
  }
  return (responseFormat);
}


/**
 * @function
 *
 */

app.use(helmet());
contentDeliveryServer.use(helmet());

// this post endpoint serves to verify input paths inside the body of the JSON file,
// it only accepts inputs from the key verifyFile
// Returns a JSON file or a list of JSON files depending on the input
app.post('/verify', urlencodedParser, (req, res) => {
  const verificationResponse = {
    filePath: '',
    exists: false,
    approved: false,
  };
  // IMPORTANT: DO NOT ERASE
  const verInput = req.body.verifyFile;


  if ((typeof verInput) === 'string' || verInput instanceof String) {
    const verInput = `./public/${req.body.verifyFile}`;
    verificationResponse.filePath = verInput;
    if (fs.existsSync(verInput)) {
      verificationResponse.exists = true;
    }
    if ((approved.CSS.includes(verInput)) || (approved.otherDirs.includes(verInput))
      || (approved.images.includes(verInput)) || (approved.js.includes(verInput))) {
      verificationResponse.approved = true;
    }
    res.send(verificationResponse);
  } else if (Array.isArray(verInput)) {
    const verInputArray = [];

    verInput.forEach((verInputElement) => {
      const arrayVerfResponse = {
        filePath: '',
        exists: false,
        approved: false,
      };
      const verifiableInputElement = `./public/${verInputElement}`;
      arrayVerfResponse.filePath = verifiableInputElement;
      if (fs.existsSync(verifiableInputElement)) {
        arrayVerfResponse.exists = true;
      }
      if ((approved.CSS.includes(verifiableInputElement))
        || (approved.images.includes(verifiableInputElement))
        || (approved.js.includes(verifiableInputElement))
        || (approved.otherDirs.includes(verifiableInputElement))) {
        arrayVerfResponse.approved = true;
      }
      verInputArray.push(arrayVerfResponse);
    });
    res.send(verInputArray);
  } else {
    res.send('not in correct file format');
  }
});


app.post('/add', urlencodedParser, (req, res) => {
  const reqInput = req.body.addFile;

  if ((typeof reqInput) === 'string' || reqInput instanceof String) {
    const response = interpretRequest(reqInput, approved, validImageFormats);
    res.send(response);
    // res.send('not ready yet');
  } else if (Array.isArray(reqInput)) {
    const reqArray = [];
    reqInput.forEach((reqArrayValue) => {
      reqArray.push(interpretRequest(reqArrayValue, approved, validImageFormats));
    });
    res.send(reqArray);
  } else {
    res.send('invalid input format');
  }
});

app.get('/list/CSS', (req, res) => {
  res.send(approved.CSS);
});
app.get('/list/images', (req, res) => {
  res.send(approved.images);
});
app.get('/list/js', (req, res) => {
  res.send(approved.js);
});
app.get('/list/other', (req, res) => {
  res.send(approved.otherDirs);
});

app.get('/add/*', (req, res) => {
  const getInput = req.url.slice(4);
  const getResponse = interpretRequest(getInput, approved, validImageFormats);
  res.send(getResponse);
});

app.listen(config.port, () => {
  console.log(`server is running on port ${config.port}`);
  console.log();
});
contentDeliveryServer.listen(config.port + 1, () => {
  console.log(`CDN server running on port ${config.port + 1}`);
});
